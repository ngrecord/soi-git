from django.conf.urls import patterns, url
from cms_email import cms_plugins

urlpatterns = patterns('',
	url(r'^(?P<list_id>\w+)/subscribe/$', cms_plugins.subscribe),
)