from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.conf import settings
from cms_email.models import EmailPlugin as EmailPluginModel
from django.utils.translation import ugettext as _
import os

from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.template import RequestContext, loader
from django.shortcuts import render_to_response, redirect
from django.contrib import messages

from soi.utils import get_mailchimp_api
import mailchimp

SLIDER_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]

class EmailPlugin(CMSPluginBase):
    name = _("Email Plugin") # Name of the plugin
    render_template = SLIDER_PATH + "/cms_email/templates/email/email.html" # template to render the plugin with
    model = EmailPluginModel # Model where data about this plugin is saved

    text_enabled = True

    def render(self, context, instance, placeholder):
        list = {}
        try:
            m = get_mailchimp_api()
            lists = m.lists.list()
            list = lists['data'][0]
        except mailchimp.Error, e:
            list = {}

        context.update({
            'placeholder': placeholder,
            'instance': instance,
            'list': list,
        })

        return context

plugin_pool.register_plugin(EmailPlugin)

def subscribe(request, list_id):
    try:
        m = get_mailchimp_api()
        m.lists.subscribe(list_id, {'email':request.POST['email']})
    except mailchimp.ListAlreadySubscribedError:
        messages.error(request,  "That email is already subscribed to the list")
        return redirect('/')
    except mailchimp.Error, e:
        messages.error(request,  'An error occurred: %s - %s' % (e.__class__, e))
        return redirect('/')

    return redirect('/?newsletter=ok', args=("list_id",))