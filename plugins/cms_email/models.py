from django.db import models
from cms.models import CMSPlugin, Page
from django.conf import settings
from tinymce import models as tinymce_models
from django.utils.translation import ugettext_lazy as _

class EmailPlugin(CMSPlugin):
	texto	 	= 	tinymce_models.HTMLField(_("texto"), null=True, blank=True)
	msg 	    = 	tinymce_models.HTMLField(_("msg"), null=True, blank=True)

	def __unicode__(self):
		return "Email Plugin"