from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.conf import settings
from models import ContactPlugin as ContactPluginModel
from django.utils.translation import ugettext as _
import os

SOI_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]

class ContactPlugin(CMSPluginBase):
    model = ContactPluginModel # Model where data about this plugin is saved
    name = _("Contact Plugin") # Name of the plugin
    render_template = SOI_PATH + "/cms_contact/templates/contact/contact.html" # template to render the plugin with

    def render(self, context, instance, placeholder):
        context.update({
            'placeholder': placeholder,
            'instance': instance,
            'contact': instance.contact.services.all,
        })
        return context

plugin_pool.register_plugin(ContactPlugin) # register the plugin