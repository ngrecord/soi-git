from django.contrib import admin
from cms.admin.placeholderadmin import PlaceholderAdmin
from models import ProcessPluginGallery, ProcessPluginPicture

class ProcessPluginPictureInline(admin.StackedInline):
    model = ProcessPluginPicture

class ProcessPluginGalleryAdmin(admin.ModelAdmin):
    inlines = [ProcessPluginPictureInline]

admin.site.register(ProcessPluginGallery, ProcessPluginGalleryAdmin)