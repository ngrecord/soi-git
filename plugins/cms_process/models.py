from cms.models import CMSPlugin, Page
from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db import transaction
from tinymce import models as tinymce_models

class ProcessPluginGallery(models.Model):
	#sdsd
	parent		 =	models.ForeignKey('self', blank=True, null=True)
	name	     = 	models.CharField(_('Nombre'), max_length=30, blank=True, null=False, default='Proceso')

	class Meta:
		verbose_name_plural = 'ProcessGallery'

	def __unicode__(self):
		return self.name

class ProcessPluginPicture(models.Model):
	gallery	     = 	models.ForeignKey(ProcessPluginGallery)
	image        = 	models.FileField(upload_to=settings.MEDIA_ROOT + "/cms_process/", blank=True, null=True)
	description	 = 	tinymce_models.HTMLField(null=True)

class ProcessPlugin(CMSPlugin):
	gallery 	 =	models.ForeignKey(ProcessPluginGallery)
	titulo	     = 	models.CharField(max_length=30, blank=True, null=True)

	def __unicode__(self):
		return "Process Plugin"