from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.conf import settings
from models import ProcessPluginGallery, ProcessPlugin as ProcessPluginModel
from django.utils.translation import ugettext as _
import os

SLIDER_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]

class ProcessPlugin(CMSPluginBase):
    model = ProcessPluginModel # Model where data about this plugin is saved
    name = _("Process Plugin") # Name of the plugin
    render_template = SLIDER_PATH + "/cms_process/templates/process/process.html" # template to render the plugin with

    def render(self, context, instance, placeholder):
    	model = ProcessPluginModel

        context.update({
            'placeholder': placeholder,
            'instance': instance,
            'gallery': instance.gallery.processpluginpicture_set.all,
        })
        return context

plugin_pool.register_plugin(ProcessPlugin) # register the plugin