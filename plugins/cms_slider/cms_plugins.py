from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.conf import settings
from models import SliderPluginGallery, SliderPlugin as SliderPluginModel
from django.utils.translation import ugettext as _
import os

SLIDER_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]

class SliderPlugin(CMSPluginBase):
    model = SliderPluginModel # Model where data about this plugin is saved
    name = _("Slider Plugin") # Name of the plugin
    render_template = SLIDER_PATH + "/cms_slider/templates/slider/slider.html" # template to render the plugin with

    def render(self, context, instance, placeholder):
    	model = SliderPluginModel

        context.update({
            'placeholder': placeholder,
            'instance': instance,
            'gallery': instance.gallery.sliderpluginpicture_set.all,
        })
        return context

plugin_pool.register_plugin(SliderPlugin) # register the plugin