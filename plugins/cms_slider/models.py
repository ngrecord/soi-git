from cms.models import CMSPlugin, Page
from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from tinymce import models as tinymce_models

class SliderPluginGallery(models.Model):
	"""page_link 	 = 	models.ForeignKey(Page, verbose_name=_("page"), null=True, limit_choices_to={'publisher_is_draft': True}, blank=True, help_text=_("If present, clicking on image will take user to specified page."))"""
	parent		 =	models.ForeignKey('self', blank=True, null=True)
	name	     = 	models.CharField(max_length=30)

	def get_absolute_url(self):
		return reverse('gallery_view', args=[self.pk])

	class Meta:
		verbose_name_plural = 'SliderGallery'

	def __unicode__(self):
		return self.name

class SliderPluginPicture(models.Model):
	gallery	     = 	models.ForeignKey(SliderPluginGallery)
	image        = 	models.FileField(upload_to=settings.MEDIA_ROOT + "/cms_slider/", blank=True, null=True)
	description	 = 	tinymce_models.HTMLField(null=True)

class SliderPlugin(CMSPlugin):
	gallery = models.ForeignKey(SliderPluginGallery)

	def __unicode__(self):
		return "Slider Plugin"