from django.contrib import admin
from cms.admin.placeholderadmin import PlaceholderAdmin
from models import SliderPluginGallery, SliderPluginPicture

class SliderPluginPictureInline(admin.StackedInline):
    model = SliderPluginPicture

class SliderPluginGalleryAdmin(admin.ModelAdmin):
    inlines = [SliderPluginPictureInline]

admin.site.register(SliderPluginGallery, SliderPluginGalleryAdmin)