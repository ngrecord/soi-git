from django.conf import settings
from django.conf.urls import patterns
from django.conf.urls.static import static

from django.conf.urls import patterns, url

from cms_soi import cms_plugins

import os

SOI_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]

urlpatterns = patterns('',
	url(r'^count/(?P<id>\d+)', cms_plugins.SoiPluginCount),
)
