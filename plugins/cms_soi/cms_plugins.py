from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.conf import settings
from models import SoiPlugin as SoiPluginModel
from django.utils.translation import ugettext as _
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.utils import simplejson as json
from cms_soi.models import SoiPluginTip, SoiPluginCount as SoiPluginCountModel
from django.contrib.auth.models import User
import os
import datetime

SOI_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]

class SoiPlugin(CMSPluginBase):
    model = SoiPluginModel # Model where data about this plugin is saved
    name = _("Soi Plugin") # Name of the plugin
    render_template = SOI_PATH + "/cms_soi/templates/soi/soi.html" # template to render the plugin with

    def render(self, context, instance, placeholder):
        request = context['request'];
        tips = SoiPluginTip.objects.all()

        """print "==========", tips
        soi_visited = None

        def addUserView(services):
            services.visited = True
            return services

        if request.user.id:
            soi_visited = SoiPluginCountModel.objects.filter(userId = request.user)
            soi_visited = map(lambda soi_visited: addUserView(soi_visited.services), soi_visited)
            soi_visited = dict((v.id,v) for v in soi_visited).values()

            tips = tips.filter(id__in=[soi_visited.id for soi_visited in soi_visited])
            #tips = tips.update(name="True")

        print "#########", tips"""

        context.update({
            'placeholder': placeholder,
            'instance': instance,
            'tips': tips,
        })
        return context

plugin_pool.register_plugin(SoiPlugin) # register the plugin

def SoiPluginCount(request, id, **httpresponse_kwargs):
    counts = SoiPluginCountModel()

    counts.name="SoiPluginCountModel"

    counts.services = SoiPluginTip.objects.get(pk=id)

    if request.GET.get('userId'):
        counts.userId=User.objects.get(pk=int(request.GET.get('userId')))

    counts.userBrowser=request.META['HTTP_USER_AGENT']
    counts.dataTime=datetime.date.today()

    counts.save()


    res = {"ok": "000: Saved Query"}
    _json = json.dumps(res)
    
    return HttpResponse(_json, content_type="application/json", **httpresponse_kwargs)
