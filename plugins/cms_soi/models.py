from cms.models import CMSPlugin, Page
from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from tinymce import models as tinymce_models
from cms_soi.widgets import ColorPickerWidget
from django.contrib.auth.models import User

class ColorField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 10
        super(ColorField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        kwargs['widget'] = ColorPickerWidget
        return super(ColorField, self).formfield(**kwargs)

class SoiPluginIcono(models.Model):
	name	     = 	models.CharField(max_length=50)
	image        = 	models.FileField(upload_to=settings.MEDIA_ROOT + "/cms_soi/", blank=True, null=True)
	classcss          = 	models.CharField(max_length=50)

	def __unicode__(self):
		return self.name

class SoiPluginService(models.Model):
	name	     = 	models.CharField(max_length=50)
	logo         = 	models.OneToOneField(SoiPluginIcono)
	description	 = 	tinymce_models.HTMLField(null=True)
	url	 		 = 	models.URLField()

	def __unicode__(self):
		return self.name

class SoiPluginTip(models.Model):
	tip	     	 = 	models.ForeignKey(SoiPluginService)
	back_color   =  ColorField(blank=True, default='#1ec87c')
	name	     = 	models.CharField(max_length=50)
	logo         = 	models.ForeignKey(SoiPluginIcono)
	description	 = 	tinymce_models.HTMLField(null=True)

	def __unicode__(self):
		return self.name

class SoiPluginApp(models.Model):
	name	     = 	models.CharField(max_length=50, null=True, blank=True)
	services     = 	models.ManyToManyField(SoiPluginService)

	class Meta:
		verbose_name_plural = 'Soi App'

	def __unicode__(self):
		return self.name

class SoiPlugin(CMSPlugin):
	soi	     	 = 	models.ForeignKey(SoiPluginApp)

	def __unicode__(self):
		return "Soi Plugin"

class SoiPluginCount(models.Model):
	name	     = 	models.CharField(max_length=50, default="Soi Counts")
	services     = 	models.ForeignKey(SoiPluginTip, null=False, blank=False)
	userId       = 	models.ForeignKey(User, null=True, blank=True)
	userBrowser  =  models.TextField(null=True, blank=True)
	dataTime     =  models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name_plural = 'Soi Counts'

	def __unicode__(self):
		return self.name

class SoiPluginSearch(SoiPluginCount):
	query	     = 	models.TextField(null=True, blank=True)

	class Meta:
		verbose_name_plural = 'Soi Search'

	def __unicode__(self):
		return self.name
