from django.contrib import admin
from cms.admin.placeholderadmin import PlaceholderAdmin
from models import SoiPluginApp, SoiPluginService, SoiPluginTip, SoiPluginIcono, SoiPluginCount, SoiPluginSearch

class SoiPluginTipInline(admin.StackedInline):
    model = SoiPluginTip

class SoiPluginAppAdmin(admin.ModelAdmin):
    pass

class SoiPluginServiceAdmin(admin.ModelAdmin):
    inlines = [SoiPluginTipInline]

class SoiPluginIconoAdmin(admin.ModelAdmin):
    pass

class SoiPluginCountAdmin(admin.ModelAdmin):
	list_display = ('services', 'userId', 'dataTime')

class SoiPluginSearchAdmin(admin.ModelAdmin):
	list_display = ('query', 'services', 'userId', 'dataTime')

admin.site.register(SoiPluginApp, SoiPluginAppAdmin)
admin.site.register(SoiPluginService, SoiPluginServiceAdmin)
admin.site.register(SoiPluginIcono, SoiPluginIconoAdmin)
admin.site.register(SoiPluginCount, SoiPluginCountAdmin)
admin.site.register(SoiPluginSearch, SoiPluginSearchAdmin)
