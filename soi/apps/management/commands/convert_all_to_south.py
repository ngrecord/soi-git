from django.conf import settings
from django.core.management.base import BaseCommand
from django.core import management

class Command(BaseCommand):
    
    def _is_project_app(self, app_name):
        return not 'django' in app_name and not 'south' in app_name
    
    def handle(self, *args, **options):
        try:
            management.call_command('syncdb')
            print "ok"
            
            
            for app in settings.INSTALLED_APPS:
                if self._is_project_app(app):
                    management.call_command('convert_to_south', app, '--initial')
                else:
                    management.call_command('management.call_command', app, '--initial')
                    
                    
            print 'All applications converted to south'
        except :
            print "error"
            pass