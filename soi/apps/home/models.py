from django.db import models
from django.conf import settings
import os

class slide(models.Model):
	image	= models.FileField(upload_to=settings.MEDIA_ROOT)
	text 	= models.TextField(max_length=300)
	title	= models.CharField(max_length=50, default="", null=False)
	def __unicode__(self):
		return "%s"%(self.title.strip())

class howitworks(models.Model):
	icon	= models.FileField(upload_to=settings.MEDIA_ROOT)
	slogan 	= models.TextField(max_length=300)
	title	= models.CharField(max_length=50, default="", null=False)
	def __unicode__(self):
		return "%s"%(self.title.strip())

class logo(models.Model):
	image	= models.FileField(upload_to=settings.MEDIA_ROOT + "/home/")

	def __unicode__(self):
		return self.image.url