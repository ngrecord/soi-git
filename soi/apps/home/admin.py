from django.contrib import admin
from soi.apps.home.models import slide, howitworks, logo

admin.site.register(slide)
admin.site.register(logo)