from django.contrib.auth.models import User, UserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

class CustomUser(User):
    objects = UserManager()
