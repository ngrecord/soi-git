from django.db import models

class Analytics(models.Model):
	nombre	= models.CharField(max_length=50, default="", null=False)
	codigo 	= models.TextField(max_length=300)
	def __unicode__(self):
		return self.nombre