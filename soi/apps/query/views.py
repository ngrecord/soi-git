from django.http import HttpResponse, Http404
from django.template import RequestContext, loader

from apiclient.discovery import build

from cms_soi.models import SoiPluginTip, SoiPluginSearch
from django.contrib.auth.models import User
from .models import AccountQuery

import datetime
import random

import pprint
from django.utils import simplejson as json


def test(request):
    html = "<html><body>Test</body></html>"
    return HttpResponse(html)


def google(request, **httpresponse_kwargs):

    accountquery = AccountQuery.objects.filter(code='google')

    print accountquery

    if not accountquery:
        raise Http404

    service = build("customsearch", "v1",
                    developerKey=accountquery[0].key)

    try:
        saveQuery(request)
    except Exception, e:
        pass

    try:
        if 's' in request.GET and request.GET['s']:
            res = service.cse().list(
                q=request.GET['s'],
                cx=accountquery[0].credential,
            ).execute()
        else:
            res = {"error": "000: Error no search on query"}
    except Exception, e:
        res = {
            "error": "000: Error on get result",
            "e": e
        }

    _json = json.dumps(res)

    return HttpResponse(_json, content_type="application/json", **httpresponse_kwargs)

def saveQuery(request):
    query = SoiPluginSearch()

    query.name="SoiPluginSearch"

    query.query=request.GET['s']

    query.services = SoiPluginTip.objects.get(pk=request.GET.get('tipId'))

    if request.GET.get('userId'):
        query.userId=User.objects.get(pk=int(request.GET.get('userId')))

    query.userBrowser=request.META['HTTP_USER_AGENT']
    query.dataTime=datetime.date.today()

    query.save()