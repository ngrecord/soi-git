from django.db import models
from django.conf import settings

class AccountQuery(models.Model):
	title	= models.CharField(max_length=50, default="", null=False)
	code 	= models.CharField(max_length=50, default="", null=False, unique=True)
	key 	= models.TextField(max_length=300)
	credential 	= models.TextField(max_length=300)
	
	def __unicode__(self):
		return "%s"%(self.title.strip())
