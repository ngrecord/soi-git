from django.conf import settings # import the settings file
from soi.apps.home.models import logo
from soi.apps.models import Analytics

def get_logo(request):
    # return the value you want as a dictionnary. you may add multiple values in there.
    _logo = ""
    try:
    	_logo = logo.objects.all()[:1].get()
    except Exception, e:
    	pass
    return {'logo': _logo}

def get_analytics(request):
    # return the value you want as a dictionnary. you may add multiple values in there.
    analytics = ""
    try:
    	analytics = Analytics.objects.all()[:1].get()
    except Exception, e:
    	pass
    return {'analytics': analytics}