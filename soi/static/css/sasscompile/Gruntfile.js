'use strict';

var path = require('path');
var lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet;

var folderMount = function folderMount(connect, point) {
  return connect.static(path.resolve(point));
};

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    cssmin: {
      main: {
        expand: true,
        cwd: '../styles/',
        src: ['*.css', '!*.min.css'],
        dest: '../styles/',
        ext: '.min.css'
      },
    },
    compass: {                  // Task
        dist: {                   // Target
          options: {              // Target options
            config: '../config.rb',
            sassDir: '../sass',
            cssDir: '../styles',
            environment: 'production',
            outputStyle: 'compressed',
          }
        },
     },
     watch: {
        options: {
          livereload: 1337,
        },
        compass: {
          files: ['../sass/*.scss'],
          tasks: ['compass', 'cssmin'],
        },
      }
  });

  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.loadNpmTasks('grunt-regarde');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-livereload');

  grunt.registerTask('live', ['watch']);
  grunt.registerTask('compile', ['compass', 'cssmin']);

};
