/*global define, angular, $, document, */

var soiApp = angular.module('soi', ['ngResource']);

soiApp.config(function ($interpolateProvider) {
    'use strict';
    $interpolateProvider.startSymbol('<%=');
    $interpolateProvider.endSymbol('%>');
});

soiApp.controller('SoiAppSimpleCtrl', function ($scope) {
    'use strict';
});

soiApp.directive('soiAppSimple', [function ($timeout) {
    'use strict';
	return {
		//priority: 0,
		//template: '<div></div>',
		//templateUrl: 'directive.html',
		//replace: true,
		//transclude: true,
		restrict: 'A',
		scope: {},
		controller: function ($scope, $element, $attrs, $transclude, google, queryObject, count) {
			$scope.count = count;
			$scope.google = google;
			$scope.queryObject = queryObject();
		},
		compile: function compile(tElement, tAttrs, transclude) {
			return {
				pre: function preLink(scope, iElement, iAttrs, controller) {
				},
                post: function postLink(scope, iElement, iAttrs, controller) {

					var $btn_open = $(iElement).find(".soi-tip-action-open");
					var $btn_close = $(iElement).find(".soi-tip-action-close");
					var $test_modal = $(iElement).find(".test-modal");

					var $canvas_graphic = $(iElement).find(".graphic").get(0);
                    var $canvas_ctx = $canvas_graphic.getContext('2d');
                    var canvas_ctx_width = $canvas_graphic.width;
                    var canvas_ctx_height = $canvas_graphic.height;
                    
                    var $canvas_renderer = null;
					var $canvas_chart = $(iElement).find(".chart");
					
					var ctx_chart = $canvas_chart.get(0).getContext("2d");

					scope.$watch('queryObject.objectsContent', function (newValue, oldValue) {
                        if (_.isEmpty(scope.queryObject.all())) return;
                        
                        scope.showResult = true;
                        
                        var maxTotalResults;
                        var maxSearchTime;
                        var maxPaginas;
                        
                        maxTotalResults = _.map(scope.queryObject.all(), function(obj, key){
                            return parseFloat(obj.query.searchInformation.totalResults);
                        })
                            
                        maxSearchTime = _.map(scope.queryObject.all(), function(obj, key){
                            return parseFloat(obj.query.searchInformation.searchTime);
                        })
                            
                        maxPaginas = _.map(scope.queryObject.all(), function(obj, key){
                            var totalResults = parseFloat(obj.query.searchInformation.totalResults)
                            var count = parseFloat(obj.query.queries.request[0].count);
                            var paginas = count != 0? parseInt(totalResults / count) : 0;
                            
                            return paginas;
                        })
                            
                        var datasets = _.map(scope.queryObject.all(), function(obj, key){
                            var totalResults = parseFloat(obj.query.searchInformation.totalResults);
                            var searchTime = parseFloat(obj.query.searchInformation.searchTime);
                            
                            var count = parseFloat(obj.query.queries.request[0].count);
                            var paginas = count != 0? parseInt(totalResults / count) : 0;
                            
                            var fullTotalResults = totalResults + " rtdo.";
                            var fullSearchTime = searchTime + " s";
                            var fullPaginas = paginas + " pág.";
                                
                            totalResults = totalResults * 100 / Math.max.apply(Math, maxTotalResults);
                            searchTime = searchTime * 100 / Math.max.apply(Math, maxSearchTime);
                            paginas = paginas * 100 / Math.max.apply(Math, maxPaginas);

                            return {
                                fillColor : "rgba("+obj.color.r+","+obj.color.g+","+obj.color.b+",0.5)",
                                strokeColor : "rgba("+obj.color.r+","+obj.color.g+","+obj.color.b+",1)",
                                pointColor : "rgba("+obj.color.r+","+obj.color.g+","+obj.color.b+",1)",
                                pointStrokeColor : "#fff",
                                data : [
                                    Math.ceil(totalResults),
                                    Math.ceil(searchTime),
                                    Math.ceil(paginas)
                                ],
                                data_full: [
                                    fullTotalResults,
                                    fullSearchTime,
                                    fullPaginas
                                ]
                            }
                        });
                            
                        var labels = ["Total Resultados", "Tiempo de Busqueda", "Número de Páginas"];
                            
                        scope.datasets = datasets.slice();
                        scope.labels = labels;
                        
                        datasets.unshift({
                            fillColor : "rgba(255,255,255,0.5)",
                            strokeColor : "rgba(255,255,255,1)",
                            pointColor : "rgba(255,255,255,1)",
                            pointStrokeColor : "#fff",
                            data : [
                                0,
                                0,
                                0
                            ]
                        })
                            
						var data = {
							labels : labels,
                            datasets : datasets
						};

						var defaults = {
                            //Boolean - If we show the scale above the chart data			
                            scaleOverlay : false,
                            //Boolean - If we want to override with a hard coded scale
                            scaleOverride : false,
                            //** Required if scaleOverride is true **
                            //Number - The number of steps in a hard coded scale
                            scaleSteps : 2,
                            //Number - The value jump in the hard coded scale
                            scaleStepWidth : 2,
                            //Number - The centre starting value
                            scaleStartValue : 2,
                            //Boolean - Whether to show lines for each scale point
                            scaleShowLine : true,
                            //String - Colour of the scale line	
                            scaleLineColor : "rgba(0,0,0,.1)",
                            //Number - Pixel width of the scale line	
                            scaleLineWidth : 30,
                            //Boolean - Whether to show labels on the scale	
                            scaleShowLabels : false,
                            //Interpolated JS string - can access value
                            scaleLabel : "<%=value%>",
                            //String - Scale label font declaration for the scale label
                            scaleFontFamily : "'Arial'",
                            //Number - Scale label font size in pixels	
                            scaleFontSize : 12,
                            //String - Scale label font weight style	
                            scaleFontStyle : "normal",
                            //String - Scale label font colour	
                            scaleFontColor : "#666",
                            //Boolean - Show a backdrop to the scale label
                            scaleShowLabelBackdrop : true,
                            //String - The colour of the label backdrop	
                            scaleBackdropColor : "rgba(255,255,255,0.75)",
                            //Number - The backdrop padding above & below the label in pixels
                            scaleBackdropPaddingY : 2,
                            //Number - The backdrop padding to the side of the label in pixels	
                            scaleBackdropPaddingX : 2,
                            //Boolean - Whether we show the angle lines out of the radar
                            angleShowLineOut : true,
                            //String - Colour of the angle line
                            angleLineColor : "rgba(0,0,0,.1)",
                            //Number - Pixel width of the angle line
                            angleLineWidth : 1,			
                            //String - Point label font declaration
                            pointLabelFontFamily : "'Arial'",
                            //String - Point label font weight
                            pointLabelFontStyle : "normal",
                            //Number - Point label font size in pixels	
                            pointLabelFontSize : 12,
                            //String - Point label font colour	
                            pointLabelFontColor : "#666",
                            //Boolean - Whether to show a dot for each point
                            pointDot : true,
                            //Number - Radius of each point dot in pixels
                            pointDotRadius : 3,
                            //Number - Pixel width of point dot stroke
                            pointDotStrokeWidth : 1,
                            //Boolean - Whether to show a stroke for datasets
                            datasetStroke : true,
                            //Number - Pixel width of dataset stroke
                            datasetStrokeWidth : 2,
                            //Boolean - Whether to fill the dataset with a colour
                            datasetFill : true,
                            //Boolean - Whether to animate the chart
                            animation : true,
                            //Number - Number of animation steps
                            animationSteps : 60,
                            //String - Animation easing effect
                            animationEasing : "easeOutQuart",
                            //Function - Fires when the animation is complete
                            onAnimationComplete : null
                        }

                        var chart = new Chart(ctx_chart);
                        chart.Radar(data, defaults);

						var urls = _.map(scope.queryObject.all(), function(obj, key) {
							var urls_query = _.map(obj.query.items, function(obj, key) {
								var link = XRegExp.exec(obj.link,
									XRegExp('^(?<scheme> [^:/?]+ ) ://   # aka protocol   \n\
                                                    (?<host>   [^/?]+  )       # domain name/IP \n\
                                                    (?<path>   [^?]*   ) \\??  # optional path  \n\
                                                    (?<query>  .*      )       # optional query', 'x')
								);
								return link.host;
							});

							return urls_query;
						});

						var nodes = _.map(urls, function(obj, key) {
							return _.map(obj, function(url) {
								return ["Consulta ("+(key+1)+")", url];
							});
						});

						var graphJSON = {
                            "nodes": _.uniq(_.flatten(nodes)),
                            "edges": _.flatten(nodes, true)
						};

						var graph = new Springy.Graph();
						graph.loadJSON(graphJSON);
                        window.node = true;
                        var layout = new Springy.Layout.ForceDirected(
                            graph,
                            200.0, // Spring stiffness
                            150.0, // Node repulsion
                            0.1 // Damping
                          );
                        
                        if(!$canvas_renderer){
                            $canvas_renderer = new Springy.Renderer(layout,
                                function clear() {
                                  $canvas_ctx.clearRect(0, 0, canvas_ctx_width, canvas_ctx_height);
                                },
                                function drawEdge(edge, p1, p2) {
                                  $canvas_ctx.save();
                                  $canvas_ctx.translate(canvas_ctx_width/2, canvas_ctx_height/2);
                            
                                  $canvas_ctx.strokeStyle = 'rgba(0,0,0,0.15)';
                                  $canvas_ctx.lineWidth = 3.0;
                            
                                  $canvas_ctx.beginPath();
                                  $canvas_ctx.moveTo(p1.x * 50, p1.y * 40);
                                  $canvas_ctx.lineTo(p2.x * 50, p2.y * 40);
                                  $canvas_ctx.stroke();
                                    
                                  $canvas_ctx.restore();
                                },
                                function drawNode(node, p) {
                                  $canvas_ctx.save();
                                  $canvas_ctx.translate(canvas_ctx_width/2, canvas_ctx_height/2);
                            
                                  $canvas_ctx.font = "bold 14px 'Roboto', 'Arial', serif";
                           
                                  var width = $canvas_ctx.measureText(node.data.label).width;
                                  var x = p.x * 50;
                                  var y = p.y * 40;
                                  $canvas_ctx.clearRect(x - width / 2.0 - 2, y - 10, width + 4, 20);
                                  $canvas_ctx.fillStyle = '#000';
                                  $canvas_ctx.fillText(node.data.label, x - width / 2.0, y + 5);
                                  
                                  $canvas_ctx.restore();
                                }
                              );
                        }
                        else{
                            $canvas_renderer.clear();
                            $canvas_renderer.layout = layout;
                        }
                        
                        window.$canvas_renderer = $canvas_renderer;
                        
                        $canvas_renderer.start();

					}, true);

					
					$btn_open.on('click', function(){
						var result;

						scope.count.resultQuery(function(data){
							//console.log(data);

							if(data.error) return;

							scope.$digest();
						}, {
							tipId: scope.tipId, 
							userId: scope.userId
						});
					});

					$test_modal.on('click', '.get-google', function(){
						var result;


						scope.google.resultQuery(function(data){
							//console.log(data);

							if(data.error) return;

							scope.queryObject.set(data);

							scope.$digest();

							$test_modal.find('[name=s]').val("");

						}, {
							tipId: scope.tipId, 
							userId: scope.userId,
							s: $test_modal.find("[name=s]").val()
						});
					});
                }
			};
		}
	};
}]);
                    
soiApp.directive('soiAppSimpleKnob', function ($timeout) {
    'use strict';
	return function (scope, iElement, iAttrs, controller) {
        iElement.ready(function() {            
            $timeout(function() {
                 $(iElement).knob();
            }, 0);
        })
    }
});

soiApp.factory('google', function($resource){
	return {
		resultQuery: function(callback, params){
			$.getJSON(location.protocol + "//" + window.location.hostname + ":" + location.port + "/q/google/?", params, 
			function(data, textStatus, jqXHR){
				callback(data);
			});
		}
	};
});

soiApp.factory('count', function($resource){
	return {
		resultQuery: function(callback, params){
			$.getJSON(location.protocol + "//" + window.location.hostname + ":" + location.port + "/cms_soi/count/"+params.tipId+"?", params, 
			function(data, textStatus, jqXHR){
				callback(data);
			});
		}
	};
});

soiApp.factory('queryObject', function($resource){
	return (function(){
		return {
			objectsContent: [],
			set: function(query){
				this.objectsContent.push({
					"query": query, 
					color: this.color()
				});
			},
			get: function(queryIndex){
				return _.indexOf(this.objectsContent, queryIndex);
			},
			all: function(){
				return this.objectsContent;
			},
			color: function(){
				return {
					r: parseInt(0 + Math.random() * 255),
					g: parseInt(0 + Math.random() * 255),
					b: parseInt(0 + Math.random() * 255)
				};
			}
		};
	});
});