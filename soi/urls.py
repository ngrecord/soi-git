from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
import os

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = i18n_patterns('',
                            url(r'^jsi18n/(?P<packages>\S+?)/$',
                                'django.views.i18n.javascript_catalog'),
                            )

#urlpatterns += staticfiles_urlpatterns()

urlpatterns += patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^lists/', include('cms_email.urls')),
    url(r'^subscribe/', include('cms_email.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^cms_soi/', include('cms_soi.urls')),
    url(r'^q/', include('soi.apps.query.urls')),

    url(r'^logout/$', 'soi.apps.aouth.views.logout'),
    url(r'', include('social.apps.django_app.urls', namespace='social')),

    url(r'^', include('cms.urls')),
)

"""
from django.contrib.staticfiles import views
if settings.DEBUG:
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
    ]
"""
#urlpatterns += static(settings.STATIC_URL, document_root=os.path.join(settings.SETTINGS_DIR, 'static/'))
